Traduisons [![Build Status](https://travis-ci.org/johntyree/traduisons-hs.svg?branch=master)](https://travis-ci.org/johntyree/traduisons-hs)
===

A Haskell port of http://github.com/johntyree/traduisons.

![Screenshot](http://i.imgur.com/gy8YLjL.png)
